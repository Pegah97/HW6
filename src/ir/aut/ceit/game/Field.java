package ir.aut.ceit.game;

import ir.aut.ceit.cards.MonsterCard;
import ir.aut.ceit.cards.SpellCard;

/**
 * Created by Asus on 5/12/2017.
 */
public class Field {
    private MonsterCard[] monsters = new MonsterCard[5];
    SpellCard[] spells = new SpellCard[5];

    public MonsterCard[] getMonsters() {
        return monsters;
    }

    public SpellCard[] getSpells() {
        return spells;
    }

    public void cardTurnEffects(Field enemyField) {
        for (int i = 0; i < 5; i++) {
            if (spells[i] != null) {
                spells[i].turnEffect(this, enemyField);
            }
            if (monsters[i] != null) {
                monsters[i].setCanAttack(true);
            }
        }
    }

    public boolean addMonsterCard(MonsterCard card) {
        for (int i = 0; i < 5; i++) {
            if (monsters[i] == null) {
                monsters[i] = card;
                return true;
            }
        }
        return false;
    }

    boolean addSpellCard(SpellCard card) {
        for (int i = 0; i < 5; i++) {
            if (spells[i] == null) {
                spells[i] = card;
                return true;
            }
        }
        return false;
    }
}
