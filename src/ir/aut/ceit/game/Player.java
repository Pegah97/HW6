package ir.aut.ceit.game;

import ir.aut.ceit.cards.Card;
import ir.aut.ceit.cards.MonsterCard;
import ir.aut.ceit.cards.SpellCard;
import ir.aut.ceit.deck.CardDeck;
import ir.aut.ceit.deck.SpecialDeck;
import ir.aut.ceit.specialcards.Special;

/**
 * Created by Asus on 5/12/2017.
 */

public class Player {
    private CardDeck mainDeck; //no getter/setter
    private SpecialDeck specialDeck; //no get/set
    private Card[] hand = new Card[5];
    private Special nextSpecial;
    private int lifePoints;

    public Player(CardDeck mainDeck, SpecialDeck specialDeck, int lifePoints) {
        this.mainDeck = mainDeck;
        this.specialDeck = specialDeck;
        this.lifePoints = lifePoints;
    }

    public Player(CardDeck mainDeck, SpecialDeck specialDeck) {
        this(mainDeck, specialDeck, 5000);
    }

    public void setHand(Card[] hand) {
        this.hand = hand;
    }

    public void setNextSpecial(Special nextSpecial) {
        this.nextSpecial = nextSpecial;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public Card[] getHand() {
        return hand;
    }

    public Special getNextSpecial() {
        return nextSpecial;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public boolean draw(int count) {
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < 5; j++) {
                if (hand[j] == null) {
                    if (mainDeck.isEmpty()) {
                        return false;
                    }
                    hand[j] = mainDeck.deal();
                }
            }
        }
        return true;
    }

    public void drawSpecialCard() {
        if (nextSpecial == null) {
            nextSpecial = (Special) specialDeck.deal();
        }
    }

    public void nextTurnPrep() {
        boolean b = draw(1);
        if (b == false) {
            lifePoints -= 500;
        }
        drawSpecialCard();
    }

    public boolean playCardFromHand(int whichCard, Field myField) {
        boolean fieldRoom = true;
        if (hand[whichCard] instanceof MonsterCard) {
            fieldRoom = myField.addMonsterCard((MonsterCard) hand[whichCard]);
        } else if (hand[whichCard] instanceof SpellCard) {
            fieldRoom = myField.addSpellCard((SpellCard) hand[whichCard]);
        }

        if ((fieldRoom == false) || (whichCard > hand.length) || (hand[whichCard] == null)) {
            return false;
        } else {
            hand[whichCard] = null;
            return true;
        }
    }

    public boolean playSpecial(Field myField) {
        boolean fieldRoom = true;
        if (nextSpecial instanceof MonsterCard) {
            fieldRoom = myField.addMonsterCard((MonsterCard) nextSpecial);
        } else if (nextSpecial instanceof SpellCard) {
            fieldRoom = myField.addSpellCard((SpellCard) nextSpecial);
        }

        if ((fieldRoom == false) || (nextSpecial == null)) {
            return false;
        } else {
            nextSpecial = null;
            return true;
        }
    }

    public void changeLifePoints(int change) {
        lifePoints += change;
    }

    public boolean isDefeated() {
        if (lifePoints <= 0) {
            return true;
        } else {
            return false;
        }
    }

}
