package ir.aut.ceit.deck;

import ir.aut.ceit.specialcards.Special;

/**
 * Created by Asus on 5/12/2017.
 */
public class SpecialDeck extends ObjectDeck {
    public SpecialDeck(Special... specials) {
        super(specials);
    }

    @Override
    public Object deal() {
        return (Special)super.deal();
    }
}
