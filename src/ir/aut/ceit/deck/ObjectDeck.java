package ir.aut.ceit.deck;

/**
 * Created by Asus on 5/12/2017.
 */
public abstract class ObjectDeck {
    private Object[] deck;
    private static int index;

    public ObjectDeck(Object[] deck) {
        this.deck = deck;
        index = deck.length;
    }

    public Object deal() {
        if (index == 0) {
            return null;
        } else {
            index--;
            return deck[index];
        }
    }

    public int size(){
        return index;
    }

    public boolean isEmpty(){
        if (index == 0) {
            return true;
        } else {
            return false;
        }
    }
}
