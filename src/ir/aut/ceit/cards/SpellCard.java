package ir.aut.ceit.cards;

import ir.aut.ceit.game.Field;

/**
 * Created by Asus on 5/12/2017.
 */
public abstract class SpellCard extends Card {
    public SpellCard(String name, String description){
        super(name,description);
    }

    public abstract void turnEffect(Field ownerField, Field enemyField);
    public abstract void destroyedEffect(Field ownerField,Field enemyField);

    @Override
    public boolean equals(Card c) {
        if ( (c instanceof SpellCard)){
            return super.equals(c);
        }
        else {
            return false;
        }
    }
}
