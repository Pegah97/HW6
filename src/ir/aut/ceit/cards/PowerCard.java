package ir.aut.ceit.cards;

import ir.aut.ceit.game.Field;

/**
 * Created by Asus on 5/12/2017.
 */
public class PowerCard extends SpellCard {
    public PowerCard() {
        super("Power Card", "Increases power of monsters by 100 each tuen");
    }

    public void turnEffect(Field ownerField, Field enemyField) {
        for (int i = 0; i < ownerField.getMonsters().length; i++) {
            ownerField.getMonsters()[i].setPower(ownerField.getMonsters()[i].getPower() + 100);
        }
    }

    public void destroyedEffect(Field ownerField, Field enemyField) {
        for (int i = 0; i < ownerField.getMonsters().length; i++) {
            ownerField.getMonsters()[i].setPower(ownerField.getMonsters()[i].getPower() - 300);
        }
    }

}
