package ir.aut.ceit.cards;

/**
 * Created by Asus on 5/12/2017.
 */
public class MonsterCard extends Card {
    private int power;
    private static int basePower; //no setter
    private boolean canAttack;

    public MonsterCard(String name, String description, int power, boolean canAttack) {
        super(name, description);
        this.power = power;
        this.canAttack = canAttack;
    }

    public MonsterCard(String name, String description, int power) {
        this(name, description, power, false);
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setCanAttack(boolean canAttack) {
        this.canAttack = canAttack;
    }

    public int getPower() {
        return power;
    }

    public int getBasePower() {
        return basePower;
    }

    public boolean getCanAttack() {
        return canAttack;
    }

    //@Override
    public boolean equals(MonsterCard c) {
        boolean b = super.equals(c);
        if ((power == c.power) && (basePower == c.getBasePower()) && b) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return super.toString() + " | " + "Power :" + power + " | " + "Can attack: " + canAttack;
    }
}
