package ir.aut.ceit.cards;

import ir.aut.ceit.game.Field;
import ir.aut.ceit.specialcards.TrapCard;

/**
 * Created by Asus on 5/12/2017.
 */
public class DestroySpell extends TrapCard {
    public DestroySpell() {
        super("Destroy Spell", "Destroys the enemy's first spell card");
    }

    public void instantEffect(Field owner, Field enemy) {

        for (int i = 0; i < enemy.getSpells().length; i++) {
            if(enemy.getSpells()[i] != null){
                enemy.getSpells()[i] = null;
                break;
            }
        }
    }
}

