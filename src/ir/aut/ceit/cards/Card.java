package ir.aut.ceit.cards;

/**
 * Created by Asus on 5/12/2017.
 */
public abstract class Card {
    private String name;
    private String description;

    public Card(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean equals(Card c) {
        if ((c.getName() == name) && (c.getDescription() == description)) {
            return true;
        } else {
            return false;
        }
    }

    public String toString(){
        return "" + name + ":" + description;
    }
}