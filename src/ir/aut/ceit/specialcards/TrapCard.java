package ir.aut.ceit.specialcards;

import ir.aut.ceit.cards.Card;

/**
 * Created by Asus on 5/12/2017.
 */
public abstract class TrapCard extends Card implements Special {
    public TrapCard(String name, String description){
        super(name,description);
    }
    @Override
    public boolean equals(Card c) {
        if ( (c instanceof TrapCard)){
            return super.equals(c);
        }
        else {
            return false;
        }
    }

}