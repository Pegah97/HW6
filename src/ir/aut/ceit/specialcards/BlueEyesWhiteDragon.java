package ir.aut.ceit.specialcards;

import ir.aut.ceit.cards.MonsterCard;
import ir.aut.ceit.game.Field;

/**
 * Created by Asus on 5/12/2017.
 */
public class BlueEyesWhiteDragon extends MonsterCard implements Special {
    public BlueEyesWhiteDragon(){
        super("Blue Eyes White Dragon", "The best card", 3000, true);
    }
    public void instantEffect(Field owner, Field enemy){

        owner.addMonsterCard(new BlueEyesWhiteDragon());
        owner.addMonsterCard(new BlueEyesWhiteDragon());
    }
}
