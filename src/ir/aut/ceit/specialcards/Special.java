package ir.aut.ceit.specialcards;

import ir.aut.ceit.game.Field;

/**
 * Created by Asus on 5/12/2017.
 */
public interface Special {
    public void instantEffect(Field owner, Field enemy);
}